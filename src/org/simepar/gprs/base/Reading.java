package org.simepar.gprs.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;
import org.simepar.gprs.j2me.FormatDate;
import org.simepar.gprs.j2me.Logger;
import org.simepar.gprs.j2me.Util;

/**
 * Classe que representa as leituras e faz o parsing das linhas recebidas na
 * serial da estacao. Esta classe é utilizada no módulo GPRS e no servidor WEB.
 *
 * @author Daniel
 */
public class Reading {
    // Nome para ser utilizado nos logs

    private static final String CLASSNAME = "Readings";
    // Flag para indicar se jah recebeu leitura do timestamp
    private boolean ready = false;
    // O horario ao qual se refere as leituras
    private long time = 0l;
    // Lista de leituras
    private Vector leituras = null; // Strings 'SENSOR VALOR'
    // Util
    private Util util = null;
    // Offset em milisegundos de 01/01/1970 a (31/12/1984)
    private static final long OFFSET = 473299200000l; //473310000000l

    /**
     * Creates a new instance of Readings
     *
     * @param util
     */
    public Reading(Util util) {
        leituras = new Vector();
        this.ready = false;
        this.util = util;
    }

    public Reading(byte[] data) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        DataInputStream dais = new DataInputStream(bais);

        this.time = dais.readLong();
        byte readings = dais.readByte();
        this.leituras = new Vector(readings);
        for (int i = 0; i < readings; i++) {
            leituras.addElement(dais.readUTF());
        }
        dais.close();
        bais.close();

        dais = null;
        bais = null;
    }

    public boolean isReady() {
        return ready;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Vector getLeituras() {
        return leituras;
    }

    /**
     * Faz o parsing das leituras e armazena-as
     *
     * @param reading Linha de leitura recebida na serial da estacao
     * @return true caso o parsing seja bem sucedido
     */
    public boolean parseAndStore(String reading) {
        // Indices utilizados para delimitar os campos da linha
        int currentPosition = 0;
        //int previousPosition=0;
        String field;

        // Os campos que devem ser obtidos
        // Será usado apenas para determinados SENSORES.. 
        // ex. se COD_ESTACAO então seta no modulo 
        String sensor = null;
//        float value = 0.0f;

        // Aguarda uma primeira linha contendo o timestamp
        // Para as estacoes do SIMEPAR, que utilizam datalogger da Sutron,
        // esta primeira linha é um float contendo os segundos em relacao
        // a 31/12/1984, com precisao de cent�simos de segundos. (atualmente ver Tc65Manager)
        if (!ready) {
            String sobra = "";

            // Encontra a primeira ocorrencia de espaco e le timestamp
            // com espaco em branco => PCD, sem => SIMEPPARLOGGER
            currentPosition = reading.indexOf(' ');

            // Verificar se data eh de PCD ou SIMEPARLOGGER
            String datahora = "";
            FormatDate fd = new FormatDate();
            if (currentPosition != -1) {
                String timestamp = reading.substring(0, currentPosition);
                // divide string ate extrair hora e minuto
                sobra = reading.substring(currentPosition + 1, reading.length()).trim();
                currentPosition = sobra.indexOf(' ');
                String hora = sobra.substring(0, currentPosition);
                String minuto = sobra.substring(currentPosition + 1, sobra.length()).trim();

                float sutronTimestamp = Float.parseFloat(timestamp);
                this.time = ((long) sutronTimestamp * 1000) + OFFSET;

                datahora = FormatDate.GetEstacaoCCLKformat(new Date(time), Integer.parseInt(hora), Integer.parseInt(minuto));

                // redefine time com data e hora corrigidos
                this.time = fd.getTimestampEstacaoFormat(datahora);
                util.setLogger("PCD");
            } else {
                this.time = fd.getTimestampSimeparLoggerFormat(reading);
                datahora = "20" + FormatDate.GetATCCLKformat(new Date(time));
                util.setLogger("SL");
            }

            // setar propriedade com ultima data recebida do modulo
            util.setProp(Util.FS_SENSOR, Util.LASTDATE, FormatDate.GetSMSCCLKformat(new Date(this.time)), true);
            Logger.info(CLASSNAME, "lastdate " + datahora);

            this.ready = true;
        } // Esta pronto para ler dados. Processa cada linha e armazena.
        else {
            // Encontra a primeira ocorrencia de espaco e le o nome do sensor            
            currentPosition = reading.indexOf(' ');
            sensor = reading.substring(0, currentPosition).trim();

            // Le a string que representa o valor
            field = reading.substring(currentPosition + 1, reading.length()).trim();

            if (sensor.equalsIgnoreCase("CodEstacao")) {
                int cod = Integer.parseInt(field);
                if (!util.readProp(Util.FS_CONF, Util.CODESTACAO, false).equals(field)) {
                    util.setProp(Util.FS_CONF, Util.CODESTACAO, String.valueOf(cod), false);
                    Logger.info(CLASSNAME, "NOVO COD ESTACAO: " + field);
                }
            }
            
            this.leituras.addElement(sensor+":"+field);
        }

        return true;
    }

    public byte[] toByteArray() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream daos = new DataOutputStream(baos);

        daos.writeLong(this.getTime());

        // Escreve o numero de leituras
        byte readings = (byte) this.getLeituras().size();
        daos.writeByte(readings);

        String leitura = "";
        for (int i = 0; i < readings; i++) {
            leitura = getLeituras().elementAt(i).toString();
            daos.writeUTF(leitura);
        }
        daos.flush();

        daos.close();
        daos = null;

        return baos.toByteArray();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("time=").append(this.getTime());
        for (int i = 0; i < this.getLeituras().size(); i++) {
            sb.append(';');
            sb.append(this.leituras.elementAt(i));
        }
        return sb.toString();
    }

    public String toSMS(String codigoEstacao) {
        StringBuffer sb = new StringBuffer();
        sb.append(codigoEstacao).append(";");
        sb.append(FormatDate.GetSMSCCLKformat(new Date(this.getTime())));
        for (int i = 0; i < this.getLeituras().size(); i++) {
            sb.append(';');
            sb.append(this.leituras.elementAt(i));
        }

        return sb.toString();
    }
}
