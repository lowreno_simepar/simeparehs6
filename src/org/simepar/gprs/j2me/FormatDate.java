/*
 * FormatDate.java
 *
 * Created on 10 de Maio de 2007, 10:37
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.simepar.gprs.j2me;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author User Net
 */
public class FormatDate {
    
    
    public FormatDate() {
    }
    
    public static String GetATCCLKformat(Date date) {
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        String dateFormatted=new String("");
        
        dateFormatted=dateFormatted.concat(String.valueOf(calendar.get(Calendar.YEAR)).substring(2,4));
        dateFormatted=dateFormatted.concat("/");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MONTH)+1));
        dateFormatted=dateFormatted.concat("/");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.DAY_OF_MONTH)));
        dateFormatted=dateFormatted.concat(",");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.HOUR_OF_DAY)));
        dateFormatted=dateFormatted.concat(":");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MINUTE)));
        dateFormatted=dateFormatted.concat(":");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.SECOND)));
                
        return dateFormatted;
    }
    
    public static String GetFileCCLKformat(Date date) {
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        String dateFormatted=new String("");
        
        dateFormatted=dateFormatted.concat(String.valueOf(calendar.get(Calendar.YEAR)).substring(2,4));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MONTH)+1));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.DAY_OF_MONTH)));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.HOUR_OF_DAY)));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MINUTE)));
                
        return dateFormatted;
    }

    public static String GetEstacaoCCLKformat(Date date, int hora, int minuto) {
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        String dateFormatted=new String("");
        
        dateFormatted=dateFormatted.concat(String.valueOf(calendar.get(Calendar.YEAR)).substring(0,4));
        dateFormatted=dateFormatted.concat("/");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MONTH)+1));
        dateFormatted=dateFormatted.concat("/");
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.DAY_OF_MONTH)));
        dateFormatted=dateFormatted.concat(","+CheckNumber(hora));
        dateFormatted=dateFormatted.concat(":"+CheckNumber(minuto));
        dateFormatted=dateFormatted.concat(":00");
                
        return dateFormatted;
    }

    public static String GetSMSCCLKformat(Date date) {
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        String dateFormatted=new String("");

        dateFormatted=dateFormatted.concat(String.valueOf(calendar.get(Calendar.YEAR)));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MONTH)+1));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.DAY_OF_MONTH)));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.HOUR_OF_DAY)));
        dateFormatted=dateFormatted.concat(CheckNumber(calendar.get(Calendar.MINUTE)));

        return dateFormatted;
    }
    
    // yyyy/MM/dd,HH:mm:ss
    public long getTimestampEstacaoFormat(String datahora) {
        Calendar calendar=Calendar.getInstance();
        
        calendar.set(Calendar.YEAR, Integer.parseInt(datahora.substring(0,4)));
        calendar.set(Calendar.MONTH, Integer.parseInt(datahora.substring(5,7))-1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datahora.substring(8,10)));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(datahora.substring(11,13)));
        calendar.set(Calendar.MINUTE, Integer.parseInt(datahora.substring(14,16)));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
                
        return calendar.getTime().getTime();
    }
    
    // dataSimeparLogger => yyMMddHHmm
    public long getTimestampSimeparLoggerFormat(String datahora) {
        Calendar calendar=Calendar.getInstance();
        
        calendar.set(Calendar.YEAR, Integer.parseInt("20"+datahora.substring(0,2)));
        calendar.set(Calendar.MONTH, Integer.parseInt(datahora.substring(2,4))-1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datahora.substring(4,6)));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(datahora.substring(6,8)));
        calendar.set(Calendar.MINUTE, Integer.parseInt(datahora.substring(8,10)));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
                
        return calendar.getTime().getTime();
    }
    
    private static String CheckNumber(int data) {
        String number = new String("");
        if (data<10) {
            number=number.concat("0");
        }
        number=number.concat(String.valueOf(data));
        
        return number;
    }
}
