/*
 *
 * Created on 6 de Setembro de 2006, 18:25
 *
 */
package org.simepar.gprs.j2me;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.rms.RecordStoreException;
import org.simepar.gprs.base.Reading;

/**
 *
 * @author Daniel
 */
public class ReadingsManager {

    private static final String CLASSNAME = "ReadingsManager";
    private Reading readings = null;
    final private Vector queue;
    private Storage storage;
    private Manager manager;
    private Util util = null;
    private Timer timer = new Timer();
    public long freq = 1;
    public long MIN_ATRASO = 62; // 1h 2min
//    private int index;

    public ReadingsManager(Storage storage, Vector queue, Manager manager, Util util) {
        this.storage = storage;
        this.queue = queue;
        this.manager = manager;
        this.util = util;
        this.freq = Long.parseLong(util.readProp(Util.FS_CONF, Util.FREQUENCIA, false));

        Logger.info(CLASSNAME, "Frequencia: " + freq + " minutos");

        // iniciando timer para LeitorSerial
        timer = new Timer();
        timer.schedule(new TestaDadosSerial(), MIN_ATRASO * 60000l);
        Logger.info(CLASSNAME, "Timer LeitorSerial " + MIN_ATRASO + " minutos");

        readings = new Reading(util);
        Logger.fine(CLASSNAME, "instanciado");
    }

    public static Vector split(String stringToSplit, String separator) {
        if (stringToSplit.length() < 1) {
            return null;
        }

        Vector stringsFound = new Vector();

        String remainingString = stringToSplit;

        while (remainingString.length() > 0) {
            int separatorStartingIndex = remainingString.indexOf(separator);

            if (separatorStartingIndex == -1) {
                // Not separators found in the remaining String. Get substring and finish
                stringsFound.addElement(remainingString);
                break;
            } else // The separator is at the beginning of the String,
            // Push the beginning at the end of separator and continue
            {
                if (remainingString.startsWith(separator)) {
                    remainingString = remainingString.substring(separator.length());
                } // The separator is present and is not the beginning, add substring and continue
                else {
                    stringsFound.addElement(remainingString.substring(0, separatorStartingIndex));
                    remainingString = remainingString.substring(separatorStartingIndex + separator.length());
                }
            }
        }

        return stringsFound;
    }

//    public void setUltimaColetaParaUDEmSMS(Reading reading) {
//        String msg = "", msgC = "";
//        boolean temComplemento = false;
//        String msgReading = reading.toString();
//        Vector tok = ReadingsManager.split(msgReading, ";");
//        // troca time=Timestamp por Data no formato para 
//        tok.setElementAt(FormatDate.GetSMSCCLKformat(new Date(reading.getTime())),0);
//        for (int i=0; i < tok.size(); i++) {
//            String msg_temp = msg+";"+tok.elementAt(i).toString();
//            if (msg_temp.length() > 160) {
////                break;
//                temComplemento = true;
//                msgC += ";"+tok.elementAt(i).toString();
//            }
//            else {
//                if (i > 0) {
//                    msg += ";"+tok.elementAt(i).toString();
//                }
//                else {
//                    String first = tok.elementAt(i).toString();
//                    msg += first;
//                    msgC += first; // se > 160
//                }
//            }
//        }
//
//        // Seta msg.. depois ver se funciona acima de 160 caracteres
//        util.setProp(Util.FS_SENSOR, "MSGSMS", msg, true);
//        util.setProp(Util.FS_SENSOR, "MSGSMSC", "", true);
//        if (temComplemento) {
//            util.setProp(Util.FS_SENSOR, "MSGSMSC", msgC, true);
//        }        
//    }
//    public String getUltimaColetaDadoSMS(Reading reading) {
//        String msg = "";
//
//        // inicia msg com data e hora
//        msg = util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + ";";
//        msg += util.readProp(Util.FS_SENSOR, Util.LASTDATE, false) + ";";
//
//        // remove ultimo "\n"
//        msg = msg.substring(0, msg.length() - 1);
//        msg += reading.toString();
//
//        return msg;
//    }
    // Divide se > 160 caracteres
    public Vector getUltimaColetaParaSMS(String msgReadingSMS) {
        Vector mensagens = new Vector();
        Vector tok = ReadingsManager.split(msgReadingSMS, ";");
        //adicona primeiro sms com estacao;datahora
        mensagens.addElement(tok.firstElement() + ";" + tok.elementAt(1));
        for (int i = 2; i < tok.size(); i++) {
            String msg_temp = mensagens.lastElement().toString() + ";"
                    + tok.elementAt(i).toString();
            if (msg_temp.length() > 160) {
//                break;
                mensagens.addElement(
                        tok.firstElement() + ";"
                        + tok.elementAt(1) + ";"
                        + tok.elementAt(i).toString());
            } else {
                mensagens.setElementAt(mensagens.lastElement() + ";"
                        + tok.elementAt(i).toString(),
                        mensagens.size() - 1);
            }
        }
        return mensagens;
    }

    public void addReading(String line) throws RecordStoreException {

        // Linha vazia
        // Fim de leitura, adiciona registro no RecordStore
        if (line == null) {

            // verifica se horario valido, se minuto nao for da frequencia, descartar leitura
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(readings.getTime()));

            // descarta se time = 0
            if (readings.getTime() == 0.0) {
                readings = new Reading(util);
                return;
            }

            System.out.println("Leitura: " + readings.toString());

            // e se nao vazio
            if (((cal.get(Calendar.MINUTE) % freq) == 0) && (readings.getLeituras().size() > 0)) {
                // antes de persistir e enviar, adiciona sinal gprs a leitura
                
                // sem tem chip
                if (manager.getSCID() != null) {
                    String sinal = manager.getSinal();
                    readings.parseAndStore("SINAL_GPRS " + sinal);                
                    readings.parseAndStore("CHIP_ID " + manager.getSCID());
                }

                if (util.readProp(Util.FS_CONF, Util.TRANSMISSAO, false).equalsIgnoreCase("GPRS")) {

                    synchronized (queue) {

                        // adiciona o dado na fila
                        queue.addElement(readings);
                        System.out.println("Leitura adicionada na fila.");

                        // persiste em arquivo se STORAGE ativo
                        if (this.storage != null) {
                            try {
                                storage.persist(readings);
                            } catch (IOException e) {
                                if (e.getMessage().trim().startsWith("arquivo de dados ja existe")) {
                                    try {
                                        storage.delete(readings);
//                                        storage.persist(readings);
                                        // se ja existe entao nao adicionar no queue
                                    } catch (Exception exp) {
                                        System.out.println("Nao possivel deletar! " + exp.getMessage());
                                        queue.removeElement(readings);
                                        return;
                                    }
                                }
                            }
                        }

                        // somente se server 'Simepar'
                        if (util.readProp(Util.FS_CONF, Util.SERVER, false).equalsIgnoreCase("simepar")) {
                            // se mais que 3 atrasados, envia ultimo da fila por sms
                            if (queue.size() > 2) {
                                Reading readingValido = null;
                                try {
                                    readingValido = (Reading) queue.lastElement(); // pega o mais atual
                                    String readingSMS = readingValido.toSMS(util.readProp(Util.FS_CONF, Util.CODESTACAO, false));
                                    Vector listSMS = this.getUltimaColetaParaSMS(readingSMS);
                                    boolean smstodos = true;
                                    for (int j = 0; j < listSMS.size(); j++) {
                                        boolean sms = manager.sendSms(
                                                util.readProp(Util.FS_CONF, Util.NUMSMS, false),
                                                listSMS.elementAt(j).toString());
                                        if (!sms) {
                                            smstodos = false;
                                        }
                                        Thread.sleep(1000l);
                                    }
                                    if (smstodos) {
                                        System.out.println("Enviou dado via SMS");
                                    } else {
                                        System.out.println("Dado nao enviado.");
                                    }
                                } catch (Exception e) {
                                    System.out.println("sendSMS " + e.getMessage());
                                }
                            }
                        }

                        // reboota modulo a cada 5 falhas de trasmissao
                        if ((queue.size() > 0) && ((queue.size() % 5) == 0)) {
                            String msg = "Telem - WARN " + util.readProp(Util.FS_CONF, Util.CODESTACAO, false)
                                    + " Reboot modulo por muitas falhas de trasmissao.";
                            manager.sendSms(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);
                            manager.rebootModule();
                        }

                        try {
                            queue.notify();  // se usar ReadingFTPSender no Main
                            //MyWatchdog.serialState = true; // recebeu da serial
                        } catch (IllegalMonitorStateException e) {
                            System.err.println("Erro ao notificar fila de dados: " + e.getMessage());
                        }
                    }

                } else if (util.readProp(Util.FS_CONF, Util.TRANSMISSAO, false).equalsIgnoreCase("SMS")) {

                    synchronized (queue) {

                        // adiciona o dado na fila
                        queue.addElement(readings);

                        Reading readingValido = null;
                        for (int i = 0; i < queue.size(); i++) {
                            try {
                                readingValido = (Reading) queue.elementAt(i);
                                String readingSMS = readingValido.toSMS(util.readProp(Util.FS_CONF, Util.CODESTACAO, false));
                                Vector listSMS = this.getUltimaColetaParaSMS(readingSMS);
                                boolean smstodos = true;
                                for (int j = 0; j < listSMS.size(); j++) {
                                    boolean sms = manager.sendSms(
                                            util.readProp(Util.FS_CONF, Util.NUMSMS, false),
                                            listSMS.elementAt(j).toString());
                                    if (!sms) {
                                        smstodos = false;
                                    }
                                    Thread.sleep(1000l);
                                }
                                if (smstodos) {
                                    System.out.println("Enviou dado via SMS");
                                    queue.removeElementAt(i);
                                } else {
                                    System.out.println("Dado nao enviado.");
                                }
                            } catch (Exception e) {
                                System.out.println("sendSMS " + e.getMessage());
                                break;
                            }

                        }

                        // reboota modulo a cada 5 falhas de trasmissao
                        System.out.println("==> " + queue.size() + " na fila de envio.");
                        if ((queue.size() > 0) && ((queue.size() % 5) == 0)) {
                            manager.rebootModule();
                        }

                    }

                } // fim transmissao sms

                // Timer: se passarem N minutos sem receber dados da serial, email sera enviado.
                timer.cancel();
                timer = new Timer();
                timer.schedule(new TestaDadosSerial(), (this.MIN_ATRASO) * 60000l);
                Logger.info(CLASSNAME, "Timer TestaLeitorSerial " + (this.MIN_ATRASO) + " minutos");

                // para novo readings
                readings = new Reading(util);
            } // fim transmissao gprs
            else {
                Logger.warning(CLASSNAME, "Fora do periodo de coleta (" + freq + " min). Dado descartado!");
                readings = new Reading(util);
            }

        } // Nova linha de dados
        // Processa a string
        else {
            readings.parseAndStore(line);
        }
    }

    public final static String encodeURL(String url) {
        String newurl = "";
        int urllen = url.length();
        for (int i = 0; i < urllen; ++i) {
            char c = url.charAt(i);
            if (((c >= 'a') && (c <= 'z'))
                    || ((c >= 'A') && (c <= 'Z'))
                    || ((c >= '0') && (c <= '9'))
                    || (c == '.') || (c == '-')
                    || (c == '*') || (c == '_')
                    || (c == '/') || (c == '~')) {
                newurl += c;
            } else if (c == ' ') {
                newurl += '+';
            } else {
                newurl += encodeChar(c);
            }
        }
        return newurl;
    }

    private static String encodeChar(char c) {
        String encchar = "%";
        encchar += Integer.toHexString((c / 16) % 16).toUpperCase();
        encchar += Integer.toHexString(c % 16).toUpperCase();
        return encchar;
    }

    public String getCLASSNAME() {
        return CLASSNAME;
    }

    class TestaDadosSerial extends TimerTask {

        public void run() {
            String log = "dados atrasados na serial";
            Logger.warning(CLASSNAME, log);
            String msg = "Telem - WARN "
                    + util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " - " + log;
            manager.sendSms(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);
            try {
                Thread.sleep(20000l);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
