/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

import com.siemens.icm.misc.Watchdog;

/**
 *
 * @author Daniel
 */
public class MyWatchdog implements Runnable {

    private final Thread _thread = new Thread(this, "watchdog");
    private boolean _loop;
    //public static boolean serialState = false;
    public static boolean ftpState = true; // inicia como true

    public void start() {
        System.out.println("Inicia watchdog - 3min");
        Watchdog.start(180);  // 3 minutos
        _loop = true;
        _thread.start();
    }

    public void stop() {
        _loop = false;
        try {
            _thread.join();
        } catch (InterruptedException e) {
            System.err.println("Exception " + e.getClass() + " : " + e.getMessage());
        }
    }

    public boolean checkProgramState() {
        //System.out.println("serial : "+serialState);
        System.out.println("ftp    : "+ftpState);
        //if (serialState && ftpState)
        if (ftpState)
            return true; // tudo ok
        else
            return false;
    }

    public void run() {
        while (_loop) {
            try {
                System.out.println("Watchdog ainda rodando...");
                if (checkProgramState()) {
                    Watchdog.kick();
                }
                Thread.sleep(20000);
            } catch (Exception ex) {
                System.err.println("Exception " + ex.getClass() + " : " + ex.getMessage());
            }
        }
    }
}
