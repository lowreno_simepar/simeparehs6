/*
 * Logger.java
 *
 * Created on 21 de Setembro de 2006, 15:54
 *
 */

package org.simepar.gprs.j2me;

import java.util.Calendar;

/**
 *
 * @author Fabio Sato, modificado por Daniel
 */
public class Logger {
    
    private final static String[] levels = {"FINE   ", "INFO   ", "WARNING", "ERROR  ", "FATAL  "};
    public final static int FINE = 0;
    public final static int INFO = 1;
    public final static int WARNING = 2;
    public final static int ERROR = 3;
    public final static int FATAL = 4;
    
    private synchronized static void log(int level, String from, String message) {
        Calendar cal = Calendar.getInstance();
        
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        sb.append(levels[level]);
        sb.append("] ");
        
        cal.setTime(Manager.getDate());
        sb.append(oneDayToString(cal));
        sb.append(" ");
        
        // Exceto para a classe Main, esta retornando somente um caracter, e
        // nao o nome da classe
        //sb.append(from.getClass().getName());
        
        sb.append(from);
        sb.append(" - ");
        sb.append(message);
        
        System.out.println(sb.toString());
        System.out.flush();
    }
    
    static String oneDayToString(Calendar oneDay) {
        return minTwoDigitString(oneDay.get(Calendar.DAY_OF_MONTH))+ "/"+
                minTwoDigitString(oneDay.get(Calendar.MONTH) + 1)+ "/"+
                oneDay.get(Calendar.YEAR)+" "+
                minTwoDigitString(oneDay.get(Calendar.HOUR_OF_DAY))+":"+
                minTwoDigitString(oneDay.get(Calendar.MINUTE))+":"+
                minTwoDigitString(oneDay.get(Calendar.SECOND));
    }
    
    private static String minTwoDigitString(int i) {
        if (i >= 0 && i < 10)
            return "0" + i;
        else
            return "" + i;
    }
    
    public static void fine(String from, String message) {
        log(FINE, from, message);
    }
    
    public static void info(String from, String message) {
        log(INFO, from, message);
    }
    
    public static void warning(String from, String message) {
        log(WARNING, from, message);
    }
    
    public static void error(String from, String message) {
        log(ERROR, from, message);
    }
    
    public static void fatal(String from, String message) {
        log(FATAL, from, message);
    }
}
