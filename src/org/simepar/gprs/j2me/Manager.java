/*
 * Manager.java
 *
 * Created on 10 de Maio de 2007, 10:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

/**
 *
 * @author Daniel
 */
import com.siemens.icm.io.*;
import java.io.*;
import com.siemens.icm.io.file.*;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.microedition.io.*;
import java.util.Date;
import java.util.Calendar;
import java.util.Vector;
import org.simepar.gprs.base.Reading;

public class Manager implements ATCommandListener {

    private static final String CLASSNAME = "Manager";
    private static final String FS = "file:///a:/";
    // Numero servico
    public static final int ID_EMAIL = 0;
    public static final int ID_FTP = 1;
    // AT-related
    private final static String URC_SMS_INCOMING = "+CMTI: \"MT\",";
    private final static String URC_SMS_REC_READ1 = "+CMGR: \"REC UNREAD\",\""; // primeira leitura
    private final static String URC_SMS_REC_READ2 = "\",";
    private final static String URC_EMAIL_OUTCOMING = "^SISW: 0, 2";
    private final static String URC_EMAIL_SIS = "^SIS: 0, 0, ";
    private final static String URC_FTP_SIS = "^SIS: 1, 0, ";
    private final static String URC_SISW_FTP_1 = "^SISW: 1, 1"; // armazenou dado
    private final static String URC_SISW_FTP_2 = "^SISW: 1, 2"; // enviou dado
    private final static String URC_CME_ERROR_NETDOWN = "+CME ERROR: network is down";
    private final static String URC_CME_ERROR_SIMNOTI = "+CME ERROR: SIM not inserted";
    private final static char AT_CTRL_Z = ((char) 26);
    private final static String SMS_TEXT_READY = ">";
    private final static String URC_CSQ = "+CSQ: ";
//    private final static String URC_CREG = "+CREG: ";
    private final static String URC_COPS = "+COPS: 0,0,";
    private final static String URC_CPIN = "+CPIN: ";
    private final static String URC_CPBR = "+CPBR: 1,";
    private final static String URC_SCID = "^SCID: ";
    // Config Rede Operadoras
    public final static String[] REDE_TIM = {"tim.br", "tim", "tim"};
    public final static String[] REDE_CLARO = {"claro.com.br", "claro", "claro"};
    public final static String[] REDE_BRT = {"brt.br", "brt", "brt"};
    public final static String[] REDE_VIVO = {"smart.m2m.vivo.com.br", "vivo", "vivo"};
    public final static String[] REDE_OI = {"gprs.oi.com.br", "oi", "oi"};
    public final static String[] REDE_OI_LIGHT = {"light.oi.com.br", "oi", "oi"};
    public String REDE_SERVICO = "TIM";
    public final boolean SENDMAIL = true;
    private Hashtable celsRegs = null;
    private final static String[] KEYS = {
        "OTAP",
        "UD", // Ultimos dados
        "RST", // Reset modulo (apenas funcoes de radio)
        "RBT", // Reboot modul
        "SIN", // Sinal gprs,
        "INFO", // Informacao da estacao (codestacao, tipo, frequencia)
        "CAD", // Cadastro codestacao
        "COM", // Config porta de comunicacao de recebimento dos dados (COM 0|1)
        "ASC", // Config saida padrao System.out (ASC 0|1)
        "OWN", // Cadastra o proprio numero na agenda do chip (OWN PHONE_NUMER) -> +DDIDDDNUMER (deprec)
        "TSM", // Config tipo de transmissao. default "GPRS" ou "SMS"(somente).
        "FREQ", // Cadastra frequencia(em minutos) aceita pela porta serial - default 1
        "STOR", // Se deve manter historico armazenado caso transmissao falhe - (default true | false)
        "SERVER" // Config do server (ftp, apn,..) para envio dos dados. ( default simepar | light )    
    };
    protected ATCommand ata;
    protected Util util;
    //private Vector qlogs;
    private static Calendar calendar;
    // FTP
    private Vector queue;
    private Vector dadosEnvio;
    private byte[] DADO;
    private Storage storage;
    private int index;
    // difference between the actual system time and the calendar received
    private static long offsetCalendar;
    // para msg de log
    private String msglog = "";
    // codigo estacao ANA cadastrado
    public String estacao = "";
    // nome estacao para titulo email
    public String estacaoTitulo = "";
    public boolean configurouOperadora = false;

    // quando usar somente at primeiro
    public Manager(ATCommand ata) {
        this.ata = ata;
        calendar = Calendar.getInstance();
    }

    // modo completo
    public Manager(ATCommand ata, Util util) {
        // num celulares registrados
        this.celsRegs = new Hashtable();
        this.celsRegs.put("4192628553", "asterisk_vivo");
        this.celsRegs.put("4185196863", "asterisk_oi");
        this.celsRegs.put("4192187692", "asterisk_old");
        this.celsRegs.put("4187364806", "fra-cla");
        this.celsRegs.put("4187387900", "fra-ges");
        this.celsRegs.put("4187195919", "fra-ges2");
        this.celsRegs.put("4187404558", "fra-mar");
        this.celsRegs.put("4187418870", "fra-mau");
        this.celsRegs.put("4187450491", "fra-moi");
        this.celsRegs.put("4187474571", "fra-wil");
        this.celsRegs.put("4187039583", "fra-ser");
        this.celsRegs.put("4187600162", "fra-ita");
        this.celsRegs.put("4199199126", "dac");
        // com 9
        this.celsRegs.put("41992628553", "asterisk_vivo");
        this.celsRegs.put("41985196863", "asterisk_oi");
        this.celsRegs.put("41992187692", "asterisk_old");
        this.celsRegs.put("41987364806", "fra-cla");
        this.celsRegs.put("41987387900", "fra-ges");
        this.celsRegs.put("41987195919", "fra-ges2");
        this.celsRegs.put("41987404558", "fra-mar");
        this.celsRegs.put("41987418870", "fra-mau");
        this.celsRegs.put("41987450491", "fra-moi");
        this.celsRegs.put("41987474571", "fra-wil");
        this.celsRegs.put("41987039583", "fra-ser");
        this.celsRegs.put("41987600162", "fra-ita");
        this.celsRegs.put("41999199126", "dac");

        this.estacao = util.readProp(Util.FS_CONF, Util.CODESTACAO, false);
        this.estacaoTitulo = this.estacao
                + "P" + util.readProp(Util.FS_CONF, Util.FREQUENCIA, false);

        this.ata = ata;
        calendar = Calendar.getInstance();
        this.util = util;

        this.configurouOperadora = this.configOperadora();
        if (this.configurouOperadora) {
            Logger.info(CLASSNAME, "Servico configurado na rede " + this.REDE_SERVICO);
        }

        this.configSMS();

        ata.addListener(this);
    }

    public boolean setDate(Date date) {
        offsetCalendar = date.getTime() - System.currentTimeMillis();
        calendar.setTime(date);
        return sendAT("at+CCLK=\"" + FormatDate.GetATCCLKformat(date) + "\"", "OK");
    }

    public static Date getDate() {
        return new Date(System.currentTimeMillis() + offsetCalendar);
    }

    public static long getTime() {
        return System.currentTimeMillis();
    }

    public String getEstacaoTitulo() {
        return this.estacaoTitulo;
    }

    private void configSMS() {
        // depende de estar registrado na rede
        // SMS recebidos irao para a memoria MT (pode setar memoria do chip ou modulo somente)
        sendAT("AT+CPMS=\"MT\"", "OK");

        // deleta ate 10 mensagens da caixa de msgs
        deleteSMSs(10);

        // SMS URC (mostra eventos)
        sendAT("AT+CNMI=3,1,0,2,1", "OK");

        // SMS modo texto
        sendAT("AT+CMGF=1", "OK");
    }

    protected synchronized boolean sendAT(String command, String expect) {
        try {
            String response = ata.send(command + "\r");
            if (response.indexOf(expect) >= 0) {
                return true;
            } else {
                Logger.error(CLASSNAME, "Erro response sendAT: " + command + " - " + response);

                String msg = "Rebootando modulo para este erro!";
                int idx = response.indexOf(URC_CME_ERROR_NETDOWN);
                if (idx >= 0) {
                    Logger.error(CLASSNAME, msg);
                    this.rebootModule();
                }

                idx = response.indexOf(URC_CME_ERROR_SIMNOTI);
                if (idx >= 0) {
                    Logger.error(CLASSNAME, msg);
                    this.rebootModule();
                }
            }

        } catch (ATCommandFailedException e) {
            Logger.error(CLASSNAME, "Erro sendAT: " + command);
            return false;
        }
        return false;
    }

    protected synchronized boolean sendAT2(String command, String expect) {
        try {
            String response = ata.send(command + "\r");
            if (response.indexOf(expect) >= 0) {
                return true;
            } else {
                Logger.error(CLASSNAME, "Erro response sendAT: " + command + " - " + response);
            }

        } catch (ATCommandFailedException e) {
            Logger.error(CLASSNAME, "Erro sendAT: " + command);
            return false;
        }
        return false;
    }

    protected synchronized String getAT(String command, String expect) {
        try {
            String response = ata.send(command + "\r");
            if (response.indexOf(expect) >= 0) {
                return response;
            }
        } catch (ATCommandFailedException e) {
            Logger.error(CLASSNAME, "Erro getAT: " + e.getMessage());
            return null;
        }
        return null;
    }

    public boolean sendSms(String num, String msg) {
        sendAT("AT+CMGF=1", "OK");
        sendAT("AT+CMGS=" + num, SMS_TEXT_READY);
        return sendAT(msg + AT_CTRL_Z, "OK");
    }

    public boolean sendSms2(String num, String msg) {
        sendAT2("AT+CMGF=1", "OK");
        sendAT2("AT+CMGS=" + num, SMS_TEXT_READY);
        return sendAT2(msg + AT_CTRL_Z, "OK");
    }

    public void deleteSMSs(int numSMSs) {
        Logger.info(CLASSNAME, "limpando ate 10msgs...");
        for (int i = 1; i <= numSMSs; i++) {
            sendAT("AT+CMGD=" + i, "OK");
        }
    }

    public boolean configConexaoGPRS(String apn, String user, String pass) {
        // Configurando conexao GPRS
        sendAT("at^sics=0,conType,GPRS0", "OK");
        sendAT("at^sics=0,inactTo,\"0\"", "OK");
        sendAT("at^sics=0,alphabet,\"1\"", "OK");
        sendAT("at^sics=0,apn,\"" + apn + "\"", "OK");
        sendAT("at^sics=0,user," + user, "OK");
        sendAT("at^sics=0,passwd," + pass, "OK");

        // Configurando servico
        return sendAT("at^sico=0", "OK"); // abre conexao
    }

    public boolean configOperadora() {
        // tenta TIM (radio/band = 3,3)

        String operadora = this.getCops();
        boolean configurou = false;

        // antes, se tiver config para FTP 'light'.. setar ftp default(rede pública)
        if (util.readProp(Util.FS_CONF, Util.SERVER, false).equalsIgnoreCase("light")) {
            util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_LIGHT, false), false);
        } else {
            util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_SIMEPAR, false), false);
        }

        if (operadora.equals("tim")) {
            configurou = this.configConexaoGPRS(REDE_TIM[0], REDE_TIM[1], REDE_TIM[2]);
            this.REDE_SERVICO = "TIM";
        } else if (operadora.equals("cla")) {
            configurou = this.configConexaoGPRS(REDE_CLARO[0], REDE_CLARO[1], REDE_CLARO[2]);
            this.REDE_SERVICO = "CLARO";
        } else if (operadora.equals("viv") || operadora.equals("tel")) { // telemig cel - rio 
            // se 'tel' -> entao usar configs da vivo (para light) / vivo com parceria com esta
            configurou = this.configConexaoGPRS(REDE_VIVO[0], REDE_VIVO[1], REDE_VIVO[2]);
            this.REDE_SERVICO = "VIVO";
        } else if (operadora.equals("oi")) {
            // se FTP 'light', dados pela rede privada 'OI' seguem para servidor interno
            if (util.readProp(Util.FS_CONF, Util.SERVER, false).equalsIgnoreCase("light")) {
                configurou = this.configConexaoGPRS(
                        REDE_OI_LIGHT[0], REDE_OI_LIGHT[1], REDE_OI_LIGHT[2]);
                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_LIGHT_OI, false), false);
            } else {
                configurou = this.configConexaoGPRS(REDE_OI[0], REDE_OI[1], REDE_OI[2]);
            }
            this.REDE_SERVICO = "OI";
        } else if (operadora.equals("bra")) {
            if (util.readProp(Util.FS_CONF, Util.SERVER, false).equalsIgnoreCase("light")) {
                configurou = this.configConexaoGPRS(
                        REDE_OI_LIGHT[0], REDE_OI_LIGHT[1], REDE_OI_LIGHT[2]);
                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_LIGHT_OI, false), false);
            } else {
                configurou = this.configConexaoGPRS(REDE_BRT[0], REDE_BRT[1], REDE_BRT[2]);
            }
            this.REDE_SERVICO = "BRT";
        } else {
            this.REDE_SERVICO = "";
        }

        System.out.println("Server: " + util.readProp(Util.FS_CONF, Util.SERVER, false));
        System.out.println("FTP: " + util.readProp(Util.FS_CONF, Util.FTP, false));

        return configurou;
    }

    public void fechaConexaoServicoGPRS(int servico) {
        sendAT("at^sisc=" + servico, "OK"); // fecha servico
        sendAT("AT^SISS=" + servico + ",srvType,\"None\"", "OK"); // zera
        sendAT("at^sicc=0", "OK"); // fecha conexao
    }

    public boolean reconfigConexaoeServicoGPRS(int servico) {
        this.fechaConexaoServicoGPRS(servico);

        boolean configurou = this.configOperadora();
        if (configurou) {
            Logger.info(CLASSNAME, "Servico configurado na rede " + this.REDE_SERVICO);
        }

        return configurou;
    }

    public void sendMail(String subj, String message) {
        if (!sendAT("at^siss=0,srvType,\"Smtp\"", "OK")) {
            Logger.error(CLASSNAME, "sendMail(): problemas no servico, nao enviar.");
            return;
        }
        sendAT("at^siss=0,alphabet,\"1\"", "OK");
        sendAT("at^siss=0,conId,\"0\"", "OK");
        sendAT("at^siss=0,user,\"gprs\"", "OK");
        sendAT("at^siss=0,passwd,\"gprs\"", "OK");
        sendAT("at^siss=0,smRcpt,\"gprs@simepar.br\"", "OK");
        sendAT("at^siss=0,smAuth,\"1\"", "OK");
        sendAT("at^siss=0,address,\"" + util.readProp(Util.FS_CONF, Util.IPSERVERMAIL, false) + "\"", "OK");
        sendAT("at^siss=0,smFrom,\"" + util.readProp(Util.FS_CONF, Util.EMAIL, false) + "\"", "OK");
        sendAT("at^siss=0,smSubj,\"" + subj + "\"", "OK");

        // abre o servico
        if (!sendAT("at^siso=0", "OK")) {
            Logger.error(CLASSNAME, "problemas no servico email");
            return;
        }
        // o email sera enviado com n bytes
        // Na versao 2: para conexoes TCP, usar "eodFlag": end of data indication flag.
        // valor = 1, indica final dos dados a serem enviados.
        int num = message.getBytes().length;
        try {
            ata.send("at^sisw=0," + num + ",1\r");
            OutputStream outStream = ata.getDataOutputStream();
            try {
                outStream.write(message.getBytes());
            } catch (IOException e) {
                msglog = "sendMail()_1: " + e.getMessage();
                Logger.error(CLASSNAME, msglog);
                this.reconfigConexaoeServicoGPRS(ID_EMAIL);
            }
        } catch (ATCommandFailedException ate) {
            msglog = "sendMail()_2: " + ate.getMessage();
            Logger.error(CLASSNAME, msglog);
            this.reconfigConexaoeServicoGPRS(ID_EMAIL);
        }
    }

    public void sendFTP(Storage storage, Vector queue, Vector dadosEnvio) throws Exception {
        this.index = queue.size() - dadosEnvio.size();
        Reading readingValido = (Reading) queue.elementAt(index);
        String nomeArquivo = util.readProp(Util.FS_CONF, Util.CODESTACAO, false)
                + "_" + FormatDate.GetFileCCLKformat(new Date(readingValido.getTime())) + ".dat";

        // Configurando servico
        if (!sendAT("at^siss=1,srvType,\"Ftp\"", "OK")) {
            Logger.error(CLASSNAME, "problemas no ftp");
            this.removeAndNotifyDadoEnvio();
            return;
        }
        sendAT("at^siss=1,alphabet,\"1\"", "OK");
        sendAT("at^siss=1,conId,\"0\"", "OK");
        sendAT("at^siss=1,address,\"ftpput://"
                + util.readProp(Util.FS_CONF, Util.FTP, false) + "/"
                + nomeArquivo + ";type=i\"", "OK");

        this.storage = storage;
        this.queue = queue;
        this.dadosEnvio = dadosEnvio;
        this.DADO = (byte[]) dadosEnvio.firstElement();
        if (!sendAT("at^siso=1", "OK")) {
            sendAT("at^sise=" + ID_FTP, "OK"); // service error
            removeAndNotifyDadoEnvio();
            this.reconfigConexaoeServicoGPRS(ID_FTP);
            throw new Exception("siso ftp - rede operadora indisponivel");
        }
        sendAT("at^sisi=" + ID_FTP, "OK"); // Check status of session
    }

    // listener interface
    /*
     * when some AT event occures, like a sms reception, a call reception.
     *
     */
    public void ATEvent(String event) {
        Logger.info(CLASSNAME, "ATEvent()\tevento = \n" + event + "(end)");
        String evento = event;

        // SMS
        int idx;
        idx = event.indexOf(URC_SMS_INCOMING);
        if (idx >= 0) {
            handleSms(event, idx);

        } // EMAIL OK
        idx = event.indexOf(URC_EMAIL_OUTCOMING);
        if (idx >= 0) {
            // envia msg e fecha servico
            Logger.info(CLASSNAME, "Email encaminhado, fechando servico e conexao...");
            sendAT("at^sisc=" + ID_EMAIL, "OK"); // fecha servico
        }

        // SIS - EMAIL (servico 0)
        idx = event.indexOf(URC_EMAIL_SIS);
        if (idx >= 0) {
            Logger.error(CLASSNAME, "EMAIL SIS: " + evento);
            this.reconfigConexaoeServicoGPRS(ID_EMAIL);
            return;
        }

        // FTP
        // caso algum evento nao esperado ocorra apos abrir o servico
        idx = event.indexOf(URC_FTP_SIS);
        if (idx >= 0) {
            Logger.error(CLASSNAME, "Erro FTP, fechando...");
            this.reconfigConexaoeServicoGPRS(ID_FTP);
            this.removeAndNotifyDadoEnvio();
        } // Armazenar e enviar dado....
        idx = event.indexOf(URC_SISW_FTP_1);
        if (idx >= 0) {
            Logger.info(CLASSNAME, "Armazenando dado FTP e enviando...");
            int num = this.DADO.length;
            try {
                ata.send("at^sisw=" + ID_FTP + "," + num + ",1\r"); // tamanho max 1500 bytes (se precisar maior fazer loop)
                OutputStream outStream = ata.getDataOutputStream();
                try {
                    outStream.write(this.DADO);
                } catch (IOException e) {
                    msglog = "ATEvent()_FTP_1: " + e.getMessage();
                    Logger.error(CLASSNAME, msglog);
                    this.reconfigConexaoeServicoGPRS(ID_FTP);
                    this.removeAndNotifyDadoEnvio();
                } finally {
                    try {
                        if (outStream != null) {
                            outStream.close();
                        }
                    } catch (IOException e) {
                        // se houver problemas, comentar essas 2 linhas
                        this.reconfigConexaoeServicoGPRS(ID_FTP);
                        this.removeAndNotifyDadoEnvio();
                    }
                }
            } catch (ATCommandFailedException ate) {
                msglog = "ATEvent()_FTP_2: " + ate.getMessage();
                Logger.error(CLASSNAME, msglog);
                this.reconfigConexaoeServicoGPRS(ID_FTP);
                this.removeAndNotifyDadoEnvio();
            }
        }

        // dado enviado, fechar servico...
        idx = event.indexOf(URC_SISW_FTP_2);
        if (idx >= 0) {
            // envia msg e fecha servi�o
            Logger.info(CLASSNAME, "FTP transferido, fechando servico...");
            sendAT("at^sisc=" + ID_FTP, "OK");

            // deletando arquivo e excluindo da fila...
            try {
                synchronized (queue) {
                    Reading reading = (Reading) queue.elementAt(index);
                    if (storage != null) {
                        storage.delete(reading);
                    }
                    queue.removeElementAt(index);
                }
            } catch (IOException e) {
                Logger.info(CLASSNAME, "Nao possivel deletar arquivo.");
            } finally {
                this.removeAndNotifyDadoEnvio();
            }
        }
    }

    public void DCDChanged(boolean SignalState) {
    }

    public void DSRChanged(boolean SignalState) {
    }

    public void RINGChanged(boolean SignalState) {
    }

    public void CONNChanged(boolean SignalState) {
    }
    /**
     *
     * @param event
     * @param idx
     */
    boolean once = false;

    private void handleSms(String event, int idx) {
        // fetch the sms index
        String smsId = "";
        int k = idx + URC_SMS_INCOMING.length();
        int j = 0;
        while (true) {
            if ((k + j + 1) > event.length()) {
                break;
            }
            String c = event.substring(k + j, k + j + 1);
            try {
                Integer.parseInt(c);
                smsId = smsId + c;
            } catch (NumberFormatException nfe) {
                break;
            }
            j++;
        }

        // Le SMS e codifica pra java string
        String msg_ori = getAT("at+cmgr=" + smsId, "OK");  // Primeira leitura, o status estarah "REC UNREAD"
        String msg = ATStringConverter.GSM2Java(msg_ori);

        int[] otidxn = new int[KEYS.length];
        boolean algum = false;
        int is = 0;
        for (int i = 0; i
                < KEYS.length; i++) {
            otidxn[i] = msg.toUpperCase().indexOf(KEYS[i]);
            if (otidxn[i] > 0) {
                algum = true;
                is = i;
                break;
            }
        }

        if (!algum) {
            System.out.println("Comando SMS nao encontrado, msg descartada... ");
            //this.enviaNotifyLogs(Logger.WARNING, CLASSNAME, msg_ori);
            return;
        }

        // Obter numero telefone
        int index = msg_ori.indexOf(URC_SMS_REC_READ1);
        System.out.println(msg_ori + " " + URC_SMS_REC_READ1 + " " + index);
        if (index < 0) {
            msglog = "problemas ao ler SMS";
            Logger.warning(CLASSNAME, msglog);
            //this.enviaNotifyLogs(Logger.WARNING, CLASSNAME, msglog);
            return;
        }
        index += URC_SMS_REC_READ1.length();
        String msg_read = msg_ori.substring(index);
        index = msg_read.indexOf(URC_SMS_REC_READ2);
        String phone_number = msg_read.substring(0, index);

        // verifica numero telefone
        String texto = "";
        // ver 11 ultimos caracteres
        if (this.celsRegs.containsKey(phone_number.substring(phone_number.length() - 11, phone_number.length()))) {
            String novamsg = "";

            int I = 0; // usar em KEYS[I]

            // OTAP
            if (otidxn[0] > 0) {
                // Config inicial OTAP
                // default: "TIM"
                String apn = REDE_TIM[0];
                String user = REDE_TIM[1];
                String pass = REDE_TIM[2];
                if (REDE_SERVICO.equals("CLARO")) {
                    apn = REDE_CLARO[0];
                    user = REDE_CLARO[1];
                    pass = REDE_CLARO[2];
                } else if (REDE_SERVICO.equals("BRT")) {
                    apn = REDE_BRT[0];
                    user = REDE_BRT[1];
                    pass = REDE_BRT[2];
                } else if (this.REDE_SERVICO.equals("VIVO")) {
                    apn = REDE_VIVO[0];
                    user = REDE_VIVO[1];
                    pass = REDE_VIVO[2];
                } else if (REDE_SERVICO.equals("OI") && !util.readProp(Util.FS_CONF, Util.SERVER, false).equals("light")) {
                    apn = REDE_OI[0];
                    user = REDE_OI[1];
                    pass = REDE_OI[2];
                } else if (REDE_SERVICO.equals("OI") && util.readProp(Util.FS_CONF, Util.SERVER, false).equals("light")) {
                    apn = REDE_OI_LIGHT[0];
                    user = REDE_OI_LIGHT[1];
                    pass = REDE_OI_LIGHT[2];
                }
                sendAT("AT^SJOTAP=\"smp\",\"http://200.19.65.115/templates/gprs/SimeparGPRS.jad\","
                        + "\"a:\",\"smp\",\"smp\",\"gprs\",\"" + apn + "\",\"" + user + "\",\"" + pass + "\"", "OK");

                sendAT("AT^SJOTAP", "OK");
                return;
            } // UD
            else if (otidxn[1] > 0) {
                I = 1;
                novamsg = util.readProp(Util.FS_SENSOR, "MSGSMS", true);
                sendSms(phone_number, novamsg);
                texto = "SMS: " + phone_number + " requisitou " + KEYS[I] + ".";

                // continuacao se existir
                String novamsgC = util.readProp(Util.FS_SENSOR, "MSGSMSC", true);
                if (!novamsgC.equals("")) {
                    sendSms(phone_number, novamsgC);
                }
            } // RST
            else if (otidxn[2] > 0) {
                this.restartModuleInFullMode();
                return;
            } // RBT => Reboot: for�a troca de chip quando "Dual Sim Card"
            else if (otidxn[3] > 0) {
                this.rebootModule();
                return;
            } // SIN ou INFO
            else if (otidxn[4] > 0 || otidxn[5] > 0) {
                //novamsg = "Sinal: "+this.getSinal()+"\n"+this.getRedeCorrente();
                novamsg = "Telem Estacao " + util.readProp(Util.FS_CONF, Util.CODESTACAO, false);
                novamsg += " Sinal: " + this.getSinal();
                novamsg += " Operadora: " + this.getCops();
                novamsg += " RedeConfig: " + this.REDE_SERVICO;
                sendSms(phone_number, novamsg);

            } // CAD ESTACAO
            // modificado para aceitar somente ARG1 => ESTACAO
            else if (otidxn[6] > 0) {
                I = 6;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valores = msg_read.toUpperCase().substring(ind1, msg_read.length() - 1);
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valores.length() - 1;

                String valor1 = "";
                String valor2 = "";
                if (TAM_CHAVE < TAM_VALORES) {
                    valores = valores.substring(TAM_CHAVE, TAM_VALORES).trim();
                    int ind2 = valores.indexOf(" ");
                    if (ind2 > -1) {
                        valor1 = valores.substring(0, ind2);
                        System.out.println("ARG1: " + valor1);
                        valor2 = valores.substring(ind2 + 1, ind2 + 2);
                        System.out.println("ARG2: " + valor2);
                    } else {
                        ind2 = valores.indexOf("OK");
                        valor1 = valores.substring(0, ind2 - 2);
                        valor1 = valor1.trim();
                        System.out.println("ARG: " + valor1);
                    }
                    util.removeProp(Util.FS_CONF, Util.CODESTACAO);
                    util.setProp(Util.FS_CONF, Util.CODESTACAO, valor1, false);
                    novamsg = "Novo codEstacao " + valor1;
                } else {
                    novamsg = KEYS[I] + " erro: " + valores;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);

                // restart em 5s
                try {
                    Thread.sleep(5000);
                    this.restartModuleInFullMode();
                } catch (Exception e) {
                }
                //return;

            } // COM 0|1 (num com)
            else if (otidxn[7] > 0) {
                I = 7;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valor = msg_read.toUpperCase().substring(ind1, msg_read.length());
                // Ex: "Com 1\nOK\n"
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valor.length();

                if (TAM_CHAVE < TAM_VALORES) {
                    int ind2 = valor.indexOf(" ");
                    if (ind2 <= -1) {
                        novamsg = KEYS[I] + " erro: " + valor;
                        //return;
                    } else {
                        valor = valor.substring(ind2 + 1, ind2 + 2).trim();
                        System.out.println("ARG: " + valor);
                        if (valor.equals("0") || valor.equals("1")) {
                            util.setProp(Util.FS_CONF, Util.COM, valor, false);
                            novamsg = KEYS[I] + " atualizado: " + valor;
                        } else {
                            novamsg = KEYS[I] + " valor invalido: " + valor;
                            //return;
                        }
                    }
                } else {
                    novamsg = KEYS[I] + " erro: " + valor;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
                //return;

                // reboota em 5s
                try {
                    Thread.sleep(5000);
                    this.restartModuleInFullMode();
                } catch (Exception e) {
                }

            } // ASC 0|1
            else if (otidxn[8] > 0) {
                I = 8;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valor = msg_read.toUpperCase().substring(ind1, msg_read.length());
                // Ex: "Com 1\nOK\n"
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valor.length();

                if (TAM_CHAVE < TAM_VALORES) {
                    int ind2 = valor.indexOf(" ");
                    if (ind2 <= -1) {
                        novamsg = KEYS[I] + " erro: " + valor;
                    } else {
                        valor = valor.substring(ind2 + 1, ind2 + 2).trim();
                        System.out.println("ARG: " + valor);
                        String stdout = "";
                        if (valor.equals("0")) {
                            stdout = "ASC0";
                            util.setProp(Util.FS_CONF, Util.STDOUT, stdout, false);
                            sendAT("at^SCFG=\"Userware/Stdout\",\"" + stdout + "\"", "OK");
                        } else if (valor.equals("1")) {
                            stdout = "ASC1";
                            util.setProp(Util.FS_CONF, Util.STDOUT, stdout, false);
                            sendAT("at^SCFG=\"Userware/Stdout\",\"" + stdout + "\"", "OK");
                        } else {
                            novamsg = KEYS[I] + " erro: " + valor;
                        }
                    }

                } else {
                    novamsg = KEYS[I] + " erro: " + valor;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " requisitou " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
            } // TSM (TRANSMISSAO) GPRS | SMS
            else if (otidxn[10] > 0) {
                I = 10;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valores = msg_read.toUpperCase().substring(ind1, msg_read.length() - 1);
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valores.length() - 1;

                String valor1 = "";
                String valor2 = "";
                boolean atualizou = false;
                if (TAM_CHAVE < TAM_VALORES) {
                    valores = valores.substring(TAM_CHAVE, TAM_VALORES).trim();
                    int ind2 = valores.indexOf(" ");
                    if (ind2 > -1) {
                        valor1 = valores.substring(0, ind2);
                        System.out.println("ARG1: " + valor1);
                        valor2 = valores.substring(ind2 + 1, ind2 + 2);
                        System.out.println("ARG2: " + valor2);
                    } else {
                        ind2 = valores.indexOf("OK");
                        valor1 = valores.substring(0, ind2 - 2);
                        valor1 = valor1.toUpperCase().trim();
                        System.out.println("ARG: " + valor1);

                        if (valor1.equals("GPRS") || valor1.equals("SMS")) {
                            util.setProp(Util.FS_CONF, Util.TRANSMISSAO, valor1, false);
                            novamsg = KEYS[I] + " atualizado: " + valores;
                            atualizou = true;
                        } else {
                            novamsg = KEYS[I] + " valor invalido: " + valor1;
                        }
                    }
                    novamsg = "Telem Estacao " + util.readProp(Util.FS_CONF, Util.CODESTACAO, false);
                    novamsg += " Tipo de trasmissao setado: " + valor1;
                    if (atualizou) {
                        novamsg += " * modulo sera reiniciado.. ";
                    }
                } else {
                    novamsg = KEYS[I] + " erro: " + valores;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
                if (atualizou) {
                    this.rebootModule();
                }
                //return;

                // FREQ MINUTO
            } else if (otidxn[11] > 0) {
                I = 11;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valores = msg_read.toUpperCase().substring(ind1, msg_read.length() - 1);
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valores.length() - 1;

                String valor = "";
                boolean atualizou = false;
                if (TAM_CHAVE < TAM_VALORES) {
                    valores = valores.substring(TAM_CHAVE, TAM_VALORES).trim();
                    int ind2 = valores.indexOf(" ");
                    if (ind2 > -1) {
                        novamsg = KEYS[I] + " erro: " + valores;
                    } else {
                        ind2 = valores.indexOf("OK");
                        valor = valores.substring(0, ind2 - 2).trim();
                        System.out.println("ARG: " + valor);
                        try {
                            int v = Integer.parseInt(valor);
                            if (v > 0) {
                                util.setProp(Util.FS_CONF, Util.FREQUENCIA, valor, false);
                                novamsg = KEYS[I] + " atualizado: " + valor;
                                atualizou = true;
                            } else {
                                novamsg = KEYS[I] + " valor invalido: " + valor;
                            }
                        } catch (NumberFormatException e) {
                            novamsg = KEYS[I] + " valor invalido: " + valor + " erro: " + e.getMessage();
                        }
                    }

                } else {
                    novamsg = KEYS[I] + " erro: " + valor;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
                //return;

                // reboota em 5s
                if (atualizou) {
                    try {
                        Thread.sleep(5000);
                        this.restartModuleInFullMode();
                    } catch (Exception e) {
                    }
                }

                // STOR (true | false)
            } else if (otidxn[12] > 0) {
                I = 12;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valores = msg_read.toUpperCase().substring(ind1, msg_read.length() - 1);
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valores.length() - 1;

                String valor = "";
                boolean atualizou = false;
                if (TAM_CHAVE < TAM_VALORES) {
                    valores = valores.substring(TAM_CHAVE, TAM_VALORES).trim();
                    int ind2 = valores.indexOf(" ");
                    if (ind2 > -1) {
                        novamsg = KEYS[I] + " erro: " + valores;
                    } else {
                        ind2 = valores.indexOf("OK");
                        valor = valores.substring(0, ind2 - 2).trim();
                        System.out.println("ARG: " + valor);
                        try {
                            if (valor.equalsIgnoreCase("true") || valor.equalsIgnoreCase("false")) {
                                util.setProp(Util.FS_CONF, Util.STORAGE, valor, false);
                                novamsg = KEYS[I] + " atualizado: " + valor;
                                atualizou = true;
                            } else {
                                novamsg = KEYS[I] + " valor invalido: " + valor;
                            }
                        } catch (NumberFormatException e) {
                            novamsg = KEYS[I] + " valor invalido: " + valor + " erro: " + e.getMessage();
                        }
                    }

                } else {
                    novamsg = KEYS[I] + " erro: " + valor;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
                //return;

                if (atualizou) {
                    // reboota em 5s
                    try {
                        Thread.sleep(5000);
                        this.restartModuleInFullMode();
                    } catch (Exception e) {
                    }
                }
            } // SERVER (simepar|light)
            else if (otidxn[13] > 0) {
                I = 13;
                int ind1 = msg_read.toUpperCase().indexOf(KEYS[I]);
                String valores = msg_read.substring(ind1, msg_read.length() - 1);
                int TAM_CHAVE = KEYS[I].length();
                int TAM_VALORES = valores.length() - 1;

                String valor = "";
                boolean atualizou = false;
                if (TAM_CHAVE < TAM_VALORES) {
                    valores = valores.substring(TAM_CHAVE, TAM_VALORES).trim();
                    int ind2 = valores.indexOf(" ");
                    if (ind2 > -1) {
                        novamsg = KEYS[I] + " erro: " + valores;
                    } else {
                        ind2 = valores.indexOf("OK");
                        valor = valores.substring(0, ind2 - 2).trim();
                        System.out.println("ARG: " + valor);

                        try {
                            if (valor.equalsIgnoreCase("simepar")) {
                                util.setProp(Util.FS_CONF, Util.SERVER, "simepar", false);
                                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_SIMEPAR, false), false);
                                novamsg = KEYS[I] + " atualizado: " + valor;
                                atualizou = true;
                            } else if (valor.equalsIgnoreCase("light")) {
                                util.setProp(Util.FS_CONF, Util.SERVER, "light", false);
                                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_LIGHT, false), false);
                                novamsg = KEYS[I] + " atualizado: " + valor;
                                atualizou = true;
                            } else {
                                novamsg = KEYS[I] + " valor invalido: " + valor;
                            }
                        } catch (NumberFormatException e) {
                            novamsg = KEYS[I] + " valor invalido: " + valor + " erro: " + e.getMessage();
                        }

                    }
                    novamsg = "Telem Estacao " + util.readProp(Util.FS_CONF, Util.CODESTACAO, false);
                    novamsg += " Servidor atualizado para " + valor;
                    if (atualizou) {
                        novamsg += " * modulo sera reiniciado.. ";
                    }
                } else {
                    novamsg = KEYS[I] + " erro: " + valores;
                    //return;
                }
                System.out.println(novamsg);

                texto = "SMS: " + phone_number + " send " + KEYS[I] + ".";
                sendSms(phone_number, texto + " " + novamsg);
                if (atualizou) {
                    // restart em 5s
                    try {
                        Thread.sleep(5000);
                        this.restartModuleInFullMode();
                    } catch (Exception e) {
                    }
                }
            }
        } else {
            texto = "SMS: " + phone_number + " nao cadastrado. " + msg_ori;
            // Enviar email somente quando tentativa de cel nao cadastrado.
            //this.sendMail(this.getEstacaoTitulo() + " - INFO", CLASSNAME + " - " + texto);
            msg = "Telem - WARN "
                    + util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " - " + texto;
            sendSms(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);
        }
        Logger.info(CLASSNAME, texto);

        deleteSMSs(1);
    }

    public void ATCommand_Configs() {
        // Watchdog
        sendAT("at^scfg=\"Userware/watchdog\",1", "OK");

        // Erros de AT mais detalhado
        sendAT("at+cmee=2", "OK");

        // limpar AT^SMSO do scfg
        sendAT("at^scfg=\"Autoexec\",0,1,1,0", "OK");
        sendAT("at^scfg=\"Autoexec\",0,1,2,0", "OK");

        // LED Status: modo de monitoramento
        //sendAT("at^ssync=1", "OK");
        // Set autoattach (conexao gprs)
        //sendAT("at^scfg=\"GPRS/ATS0/withAttach\",\"on\"", "OK");
        //sendAT("at^scfg=\"GPRS/AutoAttach\",\"disabled\"", "OK");
        // Set the program to autostart
        sendAT("at^scfg=\"Userware/Autostart\",\"\",\"1\"", "OK");

        // Where the program will be installed
        sendAT("at^scfg=\"Userware/autostart/appname\",\"\",\"a:/SimeparGPRS.jar\"", "OK");

        // Delay after power up to program start
        sendAT("at^scfg=\"Userware/autostart/delay\",\"\",\"600\"", "OK");

        // Initial Retransmission Timeout (IRT) // default 3 seg
        sendAT("at^SCFG=\"Tcp/IRT\",\"3\"", "OK");

        // Maximum Number of Retransmissions (MR) // default 10
        sendAT("at^SCFG=\"Tcp/MR\",\"10\"", "OK");

        // Overall TCP Timer for outstanding connections (tcpOT) // default 6000 seg
        sendAT("at^SCFG=\"Tcp/OT\",\"300\"", "OK");

        // Transaction/TCP (T/TCP)
        //sendAT("at^scfg=\"tcp/ttcp\",0","OK"); // "1" Enable T/TCP extension. // default 0 (disable)
        // Interface for Java System.out // default: "ASC1" (port USB side)
        // Opcoes: ASC0, ASC1, USB, NULL
        // Sera carregado de um arquivo de propriedades (posso mudar por SMS)
        // sera modificado quando util.COM for modificado: 0 => ASC1; 1 =: ASC0
        String stdout = util.readProp(Util.FS_CONF, Util.STDOUT, false);
        sendAT("at^SCFG=\"Userware/Stdout\",\"" + stdout + "\"", "OK");

        // registro automatico
        //sendAT("at+cops=0,0", "OK");
    }

    public String getSinal() {
        String var = this.getAT("AT+CSQ", "OK");
        int i1 = var.indexOf(URC_CSQ);
        int i2 = var.indexOf(",");
        var = var.substring(i1 + URC_CSQ.length(), i2 + 2);
        var = var.replace(',', '.');
        return var;
    }

    // pra futuro... utilizar o ID da operadora e nao o nome
    public String getCops() {
        String var = this.getAT("AT+COPS?", "OK");
        int i1 = var.indexOf(URC_COPS);
        int tamURC = URC_COPS.length() + 1;
        String pos = var.substring(i1 + tamURC + 2, i1 + tamURC + 3);
        if (pos.equalsIgnoreCase("\"")) { // ex: "OI"
            var = var.substring(i1 + tamURC, i1 + tamURC + 2);
        } else {
            var = var.substring(i1 + tamURC, i1 + tamURC + 3);
        }
        var = var.toLowerCase();
        return var;
    }

//    public boolean isChipReady() {
//        String var = this.getAT("AT+CPIN?", "OK");
//        int i1 = var.indexOf(URC_CPIN);
//        int tamURC = URC_CPIN.length();
//        var = var.substring(i1 + tamURC, i1 + tamURC + 5);
//        var = var.toLowerCase();
//        if (var.equals("ready")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//    public String getOwnPhoneNumber() {
//        if (isChipReady()) {
//            String var = this.getAT("AT+CPBR=1", "OK").trim();
//            int i1 = var.indexOf(URC_CPBR);
//            if (i1 > 0) {
//                int tamURC = URC_CPBR.length() + 1; // tem aspas antes do numero
//                var = var.substring(i1 + tamURC + 3, i1 + tamURC + 13).trim().toLowerCase();
//                //System.out.println("Own phone number=" + var);
//            } else {
//                var = "00000000";
//            }
//            return var;
//        } else {
//            return "0000";
//        }
//    }
//    public void setOwnPhoneNumber(String phone_numer) {
//        if (isChipReady()) {
//            if (phone_numer.length() == 10) {
//                phone_numer = "+55" + phone_numer;
//            }
//            sendAT("at+cpbw=1,\"" + phone_numer + "\",145,\"eu\"", "OK");
//        }
//    }

    /*
     * Chip Id
     */
    public String getSCID() {
        String var = this.getAT("AT^SCID", "OK");
        if (var != null) {
            int i1 = var.indexOf(URC_SCID);

            int tamURC = URC_SCID.length();
            var = var.substring(i1 + tamURC, i1 + tamURC + 20);
            var = var.toLowerCase();
            //System.out.println("SCID=" + var);
        }
        return var;
    }

    /*
     * IMEI
     */
    public String getIMEI() {
        String var = this.getAT("AT+CGSN", "OK");
        var = var.substring(10, 25);
        //System.out.println("IMEI=" + var);

        return var;
    }

    /**
     * Guarda lista de arquivos do diretorio para um vetor. Ex.: dir =
     * "file:///a:/data/"
     */
    public Vector getFilenamesFs(String dir) {
        Vector files = new Vector();
        FileConnection fc = null;
        try {
            fc = (FileConnection) Connector.open(dir, Connector.READ);
            Enumeration e = fc.list();
            while (e.hasMoreElements()) {
                files.addElement(e.nextElement());
            }
            fc.close();
            fc = null;
        } catch (Exception ioe) {
            if (fc != null && fc.isOpen()) {
                try {
                    fc.close();
                    fc = null;

                } catch (IOException ioe2) {
                }
            }
            Logger.error(CLASSNAME, "filenameFs(): " + ioe.getMessage());
        }
        return files;
    }

    public String getFilenameDataFs(String filename) {
        String data = "";
        FileConnection fc = null;
        try {
            fc = (FileConnection) Connector.open(filename);
            if (fc.exists()) {
                InputStream is = fc.openInputStream();
                int b;
                while (true) {
                    b = is.read();

                    if (b == -1) {
                        break;
                    }
                    data = data + ((char) b);
                }
                is.close();
            }
            fc.close();
            fc = null;
        } catch (IOException ioe) {
            if (fc != null && fc.isOpen()) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ioe2) {
                }
            }
            Logger.error(CLASSNAME, "filenameDataFs(): " + ioe.getMessage());
        }
        return data;
    }

    public void rebootModule() {
        // reseta configs volateis. resolve problemas no registro de rede por ex.
        this.sendAT("AT^SMSO", "OK");
    }

    public void restartModuleInFullMode() {
        this.sendAT("AT+CFUN=1", "OK");
    }

    private void removeAndNotifyDadoEnvio() {
        synchronized (dadosEnvio) {
            dadosEnvio.removeElementAt(0);
            dadosEnvio.notify();
        }
    }

    /**
     * Monta vetor de readings do FS se existirem no reinicio do programa.
     */
    public Vector getQueuesFS() {
        Vector queueFS = new Vector();
        Vector filenames = this.getFilenamesFs(Storage.FS_DATA);
        Reading readingfs = null;
        String data = "";
        try {
            Enumeration files = filenames.elements();
            while (files.hasMoreElements()) {
                data = this.getFilenameDataFs(Storage.FS_DATA + files.nextElement().toString());
                readingfs = new Reading(data.getBytes());
                queueFS.addElement(readingfs);
            }
        } catch (IOException e) {
            Logger.warning(CLASSNAME, "Problemas com a fila de transmissao inicial");
        }

        return queueFS;
    }
}
