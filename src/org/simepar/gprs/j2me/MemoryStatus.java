/*
 * MemoryStatus.java
 *
 * Created on 21 de Mar�o de 2007, 18:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.simepar.gprs.j2me;

/**
 *
 * @author Sato
 */
public class MemoryStatus extends Thread {
    
    private static final String CLASSNAME="MemoryStatus";
    
    /** Creates a new instance of MemoryStatus */
    public MemoryStatus() {
    }
    
    public void run() {
        
        while (true) {
            
            try {
                sleep(5*60000l);

                Logger.fine(CLASSNAME, "Total: "+
                        Runtime.getRuntime().totalMemory()+" | Livre: "+
                        Runtime.getRuntime().freeMemory());
                Runtime.getRuntime().gc();
            }
            catch (Exception ie)
            {
                Logger.error(CLASSNAME, " interrompida");
                return;
            }
        }
    }
}
