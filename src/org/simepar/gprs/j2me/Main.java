package org.simepar.gprs.j2me;

import com.siemens.icm.io.ATCommand;
import com.siemens.icm.io.ATCommandFailedException;
import com.siemens.icm.misc.Watchdog;
import java.util.Vector;
import javax.microedition.midlet.*;

/**
 * Esta classe e responsavel pela execucao do programa no terminal GPRS. Ela
 * cria threads que serao utilizados para ler a porta serial e enviar os dados
 * via ftp. Os dados se mantem no cartao de memoria enquanto os mesmos nao forem
 * transmitidos com sucesso.
 */
public final class Main extends MIDlet {

    private static String CLASSNAME;
    public Vector queue = new Vector();
    private Util util = null;
    private Manager Tcm = null;
    private ATCommand ATCmd = null;
    private MyWatchdog wdog = new MyWatchdog();

    /**
     * Construtor padrao
     */
    /*
     * cadastrar proprio numero no chip
     * at+cpbw=1,"+5541XXXXXXXX",145,"eu"
     */
    public Main() {
        CLASSNAME = this.getClass().getName();
        Logger.info(CLASSNAME, "Modulo Tc65i v1");

        try {
            // Inicia Watchdog
            wdog.start();

            // Create Listener em: new Manager()
            ATCmd = new ATCommand(false);
            util = new Util();

            // somente para comandos AT
            Tcm = new Manager(ATCmd);

            // Outras configs
            Logger.info(CLASSNAME, "configurando variaveis");
            if (!util.existProp(Util.FS_CONF, Util.IPSERVERMAIL)) {
                util.setProp(Util.FS_CONF, Util.IPSERVERMAIL, "200.19.65.33", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.EMAIL)) {
                util.setProp(Util.FS_CONF, Util.EMAIL, "gprs@simepar.br", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.CODESTACAO)) { //25264916 curitiba
                util.setProp(Util.FS_CONF, Util.CODESTACAO, "99999999", false);
            }
            Logger.info(CLASSNAME, "CODESTACAO: " + util.readProp(Util.FS_CONF, Util.CODESTACAO, false));
            if (!util.existProp(Util.FS_CONF, Util.FREQUENCIA)) {
                util.setProp(Util.FS_CONF, Util.FREQUENCIA, "1", false);
            }

            // Para servidores FTP
            // SIMEPAR
            if (!util.existProp(Util.FS_CONF, Util.FTP)) {
                util.setProp(Util.FS_CONF, Util.FTP_SIMEPAR, "gprs:gsmgprs@200.19.65.34:3000", false);
            }

            // LIGHT
            if (!util.existProp(Util.FS_CONF, Util.FTP_LIGHT)) {
                util.setProp(Util.FS_CONF, Util.FTP_LIGHT, "sth_amh:$th&%40mh!01@179.127.127.29", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.FTP_LIGHT_OI)) {
                util.setProp(Util.FS_CONF, Util.FTP_LIGHT_OI, "sth_amh:$th&%40mh!01@192.168.20.43", false);
            }

            // Seta FTP SIMEPAR como default (se nã existir server)
            if (!util.existProp(Util.FS_CONF, Util.SERVER)) {
                // seta default
                util.setProp(Util.FS_CONF, Util.SERVER, "simepar", false); // simepar | light
                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_SIMEPAR, false), false);
            } else if (util.readProp(Util.FS_CONF, Util.SERVER, false).equals("light")) {
                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_LIGHT, false), false);
            } else {
                util.setProp(Util.FS_CONF, Util.FTP, util.readProp(Util.FS_CONF, util.FTP_SIMEPAR, false), false);
            }

            //if (!util.existProp(Util.FS_CONF, Util.NUMSMS))
            util.setProp(Util.FS_CONF, Util.NUMSMS, "41992628553", false);

            // De preferencia: se com0 => ASC1; se com1 => ASC0
            // Setados com default
            if (!util.existProp(Util.FS_CONF, Util.COM)) {
                util.setProp(Util.FS_CONF, Util.COM, "1", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.STDOUT)) {
                util.setProp(Util.FS_CONF, Util.STDOUT, "ASC0", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.TRANSMISSAO)) {
                util.setProp(Util.FS_CONF, Util.TRANSMISSAO, "GPRS", false);
            }
            if (!util.existProp(Util.FS_CONF, Util.STORAGE)) {
                util.setProp(Util.FS_CONF, Util.STORAGE, "true", false);
            }

            util.setProp(Util.FS_SENSOR, "MSGSMS", "sem ud", true);

            // Modo completo
            Tcm = new Manager(ATCmd, util);

            // Config inicial do modulo
            Logger.info(CLASSNAME, "configurando modulo...");
            Tcm.ATCommand_Configs();

            // Envia codigoEstacao e IMEI do módulo para o servidor Asterisk
            String msg = "TELEM - INFO - CODESTACAO: "
                    + util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " IMEI:"
                    + Tcm.getIMEI();
            Tcm.sendSms2(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);

            Logger.info(CLASSNAME, "ok.");

        } catch (ATCommandFailedException e) {
            Logger.error(CLASSNAME, "Erro no construtor principal. " + e.getMessage());
            destroyApp(true);
        }
    }

    /**
     * startApp()
     */
    public void startApp() throws MIDletStateChangeException {

        String msg = "Modulo reiniciado";
        Logger.info(CLASSNAME, msg);

        ReadingsManager manager = null;
        if (util.readProp(Util.FS_CONF, Util.TRANSMISSAO, false).equalsIgnoreCase("GPRS")) {
            Storage storage = null;
            if (util.readProp(Util.FS_CONF, Util.STORAGE, false).equalsIgnoreCase("true")) {
                storage = new Storage(Tcm, util);
            }
            manager = new ReadingsManager(storage, queue, Tcm, util);

            // FTP
            ReadingsFTPSender sender = new ReadingsFTPSender(storage, queue, Tcm, util);
            Thread thread_Sender = new Thread(sender);
            thread_Sender.start();

            // carrega dados do FS
            if (util.readProp(Util.FS_CONF, Util.STORAGE, false).equalsIgnoreCase("true")) {
                CarregaLeituras fs = new CarregaLeituras(queue, Tcm);
                Thread thread_FS = new Thread(fs);
                thread_FS.start();
            }

        } else {
            manager = new ReadingsManager(null, queue, Tcm, util);
        }

        // GC e "Watch dog""
        MemoryStatus ms = new MemoryStatus();
        ms.start();

        // Cria o leitor da serial e dispara sua execucao.
        // A thread principal de execucao do programa devera ficar presa neste metodo
        LeitorSerial leitor = new LeitorSerial(manager, util);
        Thread thread_Leitor = new Thread(leitor);
        thread_Leitor.start();
        try {
            thread_Leitor.join();
            Logger.warning(CLASSNAME, "fim thread_Leitor. Reiniciando...");
        } catch (InterruptedException e) {
        }

        // Finaliza programa
        destroyApp(true);
    }

    /**
     * pauseApp()
     */
    public void pauseApp() {
        Logger.info(CLASSNAME, "pauseApp()");
    }

    /**
     * destroyApp()
     *
     * This is important. It closes the app's RecordStore
     *
     * @param cond true if this is an unconditional destroy false if it is not
     * currently ignored and treated as true
     */
    public void destroyApp(boolean cond) {
        Watchdog.start(300);
        Logger.info(CLASSNAME, "Para cancelar reset, digite: ");
        Logger.info(CLASSNAME, "at^scfg=\"Userware/Watchdog\",0");

        // avisar que a aplicacao parou
        String msg = "Aplicacao parou";
        Logger.fatal(CLASSNAME, msg);
        notifyDestroyed();
    }
}
