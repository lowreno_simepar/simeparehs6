/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

import java.util.Vector;

/**
 *
 * @author Daniel
 */
public class CarregaLeituras implements Runnable {

    private String CLASSNAME;
    private Manager tcm;
    private final Vector queue;

    public CarregaLeituras(Vector queue, Manager tcm) {
        this.CLASSNAME = this.getClass().getName();
        this.tcm = tcm;
        this.queue = queue;
    }

    public void run() {
        Vector recup = tcm.getQueuesFS();
        if (recup.size() > 0) {
            synchronized (queue) {
                for (int i = 0; i < recup.size(); i++) {
                    queue.addElement(recup.elementAt(i));
                }
            }
            Logger.info(CLASSNAME, recup.size() + " pacote(s) de leitura(s) recuperada(s).");
        } else {
            Logger.info(CLASSNAME, "Nenhuma leituraF anterior no FS.");
        }
    }
}
