/*
 * ReadingsHTTPSender
 *
 * Created on 18 de Setembro de 2006, 14:08
 *
 */
package org.simepar.gprs.j2me;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.Vector;
import org.simepar.gprs.base.Reading;

/**
 * Transmite os dados via HTTP
 *
 * @author Daniel
 */
public class ReadingsFTPSender implements Runnable {

    private static final String CLASSNAME = "ReadingsFTPSender";
    private final Vector queue;
    private Manager manager = null;
    private Util util = null;
    private Storage storage;
    private String msglog = "";
    private Timer timer = new Timer();
    private long MIN_ATRASO = 65; // 1h e 5 min

    /** Creates a new instance of HTTPObserver */
    public ReadingsFTPSender(Storage storage, Vector queue, Manager manager, Util util) {
        Logger.fine(CLASSNAME, "construtor chamado");
        this.storage = storage;
        this.queue = queue;
        this.manager = manager;
        this.util = util;

        // iniciando timer para FTP
        MyWatchdog.ftpState = true;
        timer = new Timer();
        timer.schedule(new TestaEnvioFTP(manager, util), MIN_ATRASO * 60000l);
        Logger.info(CLASSNAME, "Timer TestaEnvioFTP " + MIN_ATRASO + " minutos");
    }

    public void run() {

        boolean forever = true;
        while (forever) {
            Logger.info(CLASSNAME, "aguardando transmissao");

            // Cria um OutputStream apropriado para a transmiss�o dos bytes
            ByteArrayOutputStream baos = null;
            DataOutputStream daos = null;
            Vector dadosEnvio = new Vector();
            Reading readingValido = null;

            // Escreve codigoEstacao em todos os dados(Reading) para envio
            try {
                synchronized (queue) {

                    try {
                        queue.wait();
                    } catch (InterruptedException e) {}

                    byte[] data = new byte[0];
                    for (int i = 0; i < queue.size(); i++) {
                        baos = new ByteArrayOutputStream();
                        daos = new DataOutputStream(baos);

                        daos.writeInt(Integer.parseInt(util.readProp(Util.FS_CONF, Util.CODESTACAO, false)));
                        readingValido = (Reading) queue.elementAt(i);
                        data = readingValido.toByteArray();
                        // escreve o tamanho em bytes do array
                        daos.writeInt(data.length);
                        // manda o array
                        daos.write(data);
                        dadosEnvio.addElement(baos.toByteArray());
                    }
                    daos.close();
                    daos = null;
                    baos.close();
                    baos = null;
                }
            } catch (Exception e) {
                msglog = "run()_1: " + e.getMessage();
                Logger.error(CLASSNAME, msglog);
            } finally {
                if (daos != null) {
                    try {
                        daos.close();
                    } catch (IOException ioe) {
                        System.err.println(ioe.getMessage());
                    }
                }
                if (baos != null) {
                    try {
                        baos.close();
                    } catch (IOException ioe) {
                        System.err.println(ioe.getMessage());
                    }
                }
            }

            int numEnvio = dadosEnvio.size();
            for (int i = 0; i < numEnvio; i++) {
                synchronized (dadosEnvio) {
                    try {
                        manager.sendFTP(storage, queue, dadosEnvio);
                        // aguarda notificacoes na fila
                        dadosEnvio.wait();
                    } catch (InterruptedException ie) {
                        Logger.error(CLASSNAME, ie.getMessage());
                        continue;
                    } catch (Exception e) {
                        System.out.println("sendFTP " + e.getMessage());
                        System.out.println("refazer servico...");
                        manager.reconfigConexaoeServicoGPRS(Manager.ID_FTP);
                        break;
                    }
                }
            }
            Logger.info(CLASSNAME, "fim transmissao.");
            MyWatchdog.ftpState = true;
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TestaEnvioFTP(manager, util), (this.MIN_ATRASO) * 60000l);
            Logger.info(CLASSNAME, "Timer TestaEnvioFTP " + (this.MIN_ATRASO) + " minutos");
        }
    }
}
