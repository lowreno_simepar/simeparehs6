/*
 * PersistenceManager.java
 *
 * Created on 26 de Março de 2007, 14:53
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.simepar.gprs.j2me;

import com.siemens.icm.io.file.FileConnection;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import org.simepar.gprs.base.Reading;

/**
 *
 * @author Daniel
 */
public class Storage {
    
    private static final String CLASSNAME = "Storage";
    public static final String FS_DATA = "file:///a:/data/";
    
    private Util util = null;
    private Manager manager = null;
    
    /** Creates a new instance of PersistenceManager */
    public Storage(Manager manager, Util util) {
        this.manager = manager;
        this.util = util;
        
        // inicia dir se nao existir
        FileConnection fc = null;
        try {
            fc = (FileConnection) Connector.open(FS_DATA);
            if (!fc.exists()) {
                fc.mkdir();
            }
            fc.close();
            fc = null;
        } catch (IOException ioe) {
            if (fc != null && fc.isOpen()) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ioe2) {}
            }
            Logger.error(CLASSNAME, "Storage(): "+ioe.getMessage());
        }
    }
    
    /**
     * Armazena os dados de uma leitura em um arquivo e retorna
     * a porcentagem de uso
     */
    public void persist(Reading readings) throws IOException {
        String filename = createFileName(readings);
        Logger.fine(CLASSNAME, " persistindo dados no arquivo: " + filename);
        
        FileConnection fc = (FileConnection) Connector.open(filename);
        
        if (fc.availableSize() < 1024) { // menos de 1 kbytes disponivel no flash
            String msg = "sem espaco ou flash nao esta acessivel: "+fc.availableSize();
            fc.close();
            fc = null;
            Logger.error(CLASSNAME, "persist(): "+msg);
            manager.sendMail(manager.getEstacaoTitulo(), msg);
            throw new IOException(msg);
        }
        
        if (fc.exists()) {
            String msg = "arquivo de dados ja existe: " + filename + ". Deletando..";
            fc.delete();
            fc.close();
            fc = null;
            Logger.error(CLASSNAME, "persist(): "+msg);
            fc = (FileConnection) Connector.open(filename);
            Logger.info(CLASSNAME, "persist(): Reabrindo arquivo.");
            //throw new IOException(msg);
        }
               
        fc.create();
        DataOutputStream dos = fc.openDataOutputStream();
        dos.write(readings.toByteArray());
        
        dos.close();        
        fc.close();
        dos = null;
        fc = null;
    }
    
    /**
     * Remove os dados
     */
    public void delete(Reading readings) throws IOException {
        String filename = createFileName(readings);
        Logger.fine(CLASSNAME, " removendo arquivo de dados: " + filename);
        
        FileConnection fc = (FileConnection) Connector.open(filename);
        if (!fc.exists()) {
            String msg = "delete(): arquivo nao existe: " + filename;
            Logger.error(CLASSNAME, msg);
            fc.close();
            fc = null;
            
            throw new IOException(msg);
        }       
        fc.delete();
        fc.close();
        fc = null;
    }
    
    private String createFileName(Reading readings) {
        StringBuffer sb = new StringBuffer();
        sb.append(FS_DATA);
        sb.append(readings.getTime());
        sb.append(".dat");
        return sb.toString();
    }
    
}
