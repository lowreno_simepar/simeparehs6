/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

import java.util.TimerTask;

/**
 *
 * @author Daniel
 */
class EnviaInfoChip extends TimerTask {

    String CLASSNAME;
    Manager tc65;
    Util util;

    public EnviaInfoChip(Manager tc65, Util util) {
        this.CLASSNAME = this.getClass().getName();
        this.tc65 = tc65;
        this.util = util;
    }

    public void run() {
        // Atualiza cel e outras infos do modulo no servidor SMS (Asterisk)
        String msg = "SIMEPAR_GPRS_TELEM ";
        if (tc65.configurouOperadora) {
            msg += util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " INFO "
                    + tc65.getSinal() + " " + tc65.getCops();
        } else {
            msg += util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " ERROR "
                    + tc65.getSinal() + " - Nao configurou na rede op.";
        }
        tc65.sendSms(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);
        try {
            Thread.sleep(30000l);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
