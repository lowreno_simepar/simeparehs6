/*
 * Util.java
 *
 * Created on 16 de Maio de 2007, 17:30
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

import com.siemens.icm.io.file.FileConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import javax.microedition.io.Connector;

/**
 *
 * @author Daniel
 */
public class Util {

    private static final String CLASSNAME = "Util";
    private final static String FS = "file:///a:/";
    public final static String FS_SENSOR = "file:///a:/props/sensor/";
    public final static String FS_CONF = "file:///a:/props/conf/";
    public final static String FS_TMP = "file:///a:/props/tmp/";
    // Configs
    public final static String IPSERVERMAIL = "IPSERVERMAIL";
    public final static String EMAIL = "EMAIL";
    public final static String CODESTACAO = "CODESTACAO";
    public final static String FREQUENCIA = "FREQUENCIA";
    public final static String SERVER = "SERVER";
    public final static String FTP = "FTP";
    public final static String FTP_SIMEPAR = "FTP_SIMEPAR";
    public final static String FTP_LIGHT = "FTP_LIGHT";
    public final static String FTP_LIGHT_OI = "FTP_LIGHT_OI";
    public final static String COM = "COM";
    public final static String STDOUT = "STDOUT";
    public final static String NUMSMS = "NUMSMS";
    public final static String LASTDATESMS = "LASTSMS"; // ultimo sms - data do sistema
    public final static String TRANSMISSAO = "TRANSMISSAO"; // GPRS | SMS
    public final static String STORAGE = "STORAGE"; // boolean
    
    // sensor
    public final static String LASTDATE = "lastdate"; // ultimo dado
    private Hashtable propertiesTable;

    /** Creates a new instance of Util */
    public Util() {
        propertiesTable = new Hashtable();
        this.makeDIR(FS_CONF);
        this.makeDIR(FS_SENSOR);
        this.makeDIR(FS_TMP);
    }

    public void makeDIR(String DIR) {
        // inicia dir se nao existir
        FileConnection fc = null;
        try {
            fc = (FileConnection) Connector.open(DIR);
            if (!fc.exists()) {
                fc.mkdir();
            }
            fc.close();
            fc = null;
        } catch (IOException ioe) {
            if (fc != null && fc.isOpen()) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ioe2) {
                }
            }
            Logger.error(CLASSNAME, "makeDIR(): " + ioe.getMessage());
        }
    }

    public static String getFlashPath() {
        return FS;
    }

    public String readProp(String FS_DIR, String prop, boolean justMemory) {

        if (justMemory) {
            if (propertiesTable.containsKey(prop)) {
                return (String) propertiesTable.get(prop);
            } else {
                return "";
            }

        } else {
            // check if value is already in the table
            if (propertiesTable.containsKey(prop)) {
                return (String) propertiesTable.get(prop);

            } else { // read property from flash
                String val = null;
                FileConnection fc = null;
                try {
                    fc = (FileConnection) Connector.open(
                            FS_DIR + prop, Connector.READ);
                    if (fc.exists()) {
                        InputStream is = fc.openInputStream();
                        val = "";
                        int b;
                        while (true) {
                            b = is.read();
                            if (b == -1) {
                                break;
                            }
                            val = val + ((char) b);
                        }
                        is.close();
                    }
                    fc.close();
                    fc = null;
                } catch (IOException ioe) {
                    if (fc != null && fc.isOpen()) {
                        try {
                            fc.close();
                            fc = null;
                        } catch (IOException ioe2) {
                        }
                    }
                    Logger.error(CLASSNAME, "readProp(): " + ioe.getMessage());
                }

                // add the new prop in the properties table
                if (val != null) {
                    propertiesTable.put(prop, val);
                }
                
                return val;
            }
        }
    }

    public String setProp(String FS_DIR, String prop, String val, boolean justMemory) {

        if (!justMemory) {
            // write to flash
            FileConnection fc = null;
            OutputStream output = null;
            try {
                fc = (FileConnection) Connector.open(FS_DIR + prop, Connector.READ_WRITE);

                if (!fc.exists()) {
                    fc.create();
                }
                else {
                    fc.delete();
                    fc.create();
                }
                
                output = fc.openOutputStream();
                output.write(val.getBytes());
                output.flush();
                output.close();

                output.close();
                fc.close();

                output = null;
                fc = null;
            } catch (IOException e) {
                Logger.error(CLASSNAME, "setProp(): " + e.getMessage());
                try {
                    if (fc != null && fc.isOpen()) {
                        fc.close();
                    }
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException e2) {
                } finally {
                    fc = null;
                    output = null;
                }
            }
        }

        propertiesTable.put(prop, val);

        return val;
    }

    public boolean existProp(String FS_DIR, String prop) {
        // check if value is already in the table
        if (propertiesTable.containsKey(prop)) {
            return true;

        } else { // read property from flash
            String val = null;
            FileConnection fc = null;
            InputStream is = null;
            try {
                fc = (FileConnection) Connector.open(
                        FS_DIR + prop, Connector.READ);
                if (fc.exists()) {
                    is = fc.openInputStream();
                    val = "";
                    int b;
                    while (true) {
                        b = is.read();
                        if (b == -1) {
                            break;
                        }
                        val = val + ((char) b);
                    }
                    is.close();
                    is = null;
                }
                fc.close();
                fc = null;
            } catch (IOException e) {
                try {
                    if (fc != null && fc.isOpen()) {
                        fc.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e2) {
                } finally {
                    fc = null;
                    is = null;
                }
                Logger.error(CLASSNAME, "existProp(): " + e.getMessage());
            }

            // add the new prop in the properties table
            if (val != null) {
                propertiesTable.put(prop, val);
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * Holds value of property logger.
     * PCD ou SL (SIMEPARLOGGER)
     */
    private String logger = "PCD";

    /**
     * Getter for property logger.
     * @return Value of property logger.
     */
    public String getLogger() {
        return this.logger;
    }

    /**
     * Setter for property logger.
     * @param logger New value of property logger.
     */
    public void setLogger(String logger) {
        this.logger = logger;
    }

    public void removeProp(String FS_DIR, String prop) {

        // write to flash
        FileConnection fc = null;
        try {
            fc = (FileConnection) Connector.open(FS_DIR + prop, Connector.READ_WRITE);

            if (fc.exists()) {
                fc.delete();
            }

            fc.close();
            fc = null;
        } catch (IOException e) {
            Logger.error(CLASSNAME, "setProp(): " + e.getMessage());
            try {
                if (fc != null && fc.isOpen()) {
                    fc.close();
                }
            } catch (IOException e2) {
            } finally {
                fc = null;
            }
        }
        propertiesTable.remove(prop);
    }
}
