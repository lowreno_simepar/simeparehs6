/*
 * LeitorSerial.java
 *
 */

package org.simepar.gprs.j2me;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;

/**
 *
 * @author Sato, Daniel
 */
public class LeitorSerial implements Runnable {
    
    private static final String CLASSNAME = "LeitorSerial";
    private Util util = null;
    
    // Carregar de um arquivo de propriedades (posso mudar por SMS)
    private int numcom = 1;
    private String commDesc = null;
    
    CommConnection comm = null;
    ReadingsManager obs = null;
    InputStream input = null;
    OutputStream output = null;
    
    /** Creates a new instance of LeitorSerial */
    public LeitorSerial(ReadingsManager obs, Util util) {
        this.util = util;
        this.numcom = Integer.parseInt(util.readProp(Util.FS_CONF, Util.COM,false));
        this.commDesc = "comm:com"+this.numcom+";blocking=on;baudrate=9600";
        
        Logger.fine(CLASSNAME, "construtor chamado");
        this.obs = obs;

        //MyWatchdog.serialState = true;
    }
    
    public void run() {
        Logger.fine(CLASSNAME, "run()");
        try {
            // Abre a porta serial
            Logger.fine(CLASSNAME, "abrindo porta serial: com"+this.numcom);
            comm = (CommConnection) Connector.open(commDesc);
        } catch (IOException ex) {
            Logger.error(CLASSNAME, "nao foi possivel abrir a porta de comunicacoes: "+ex.getMessage());
            return;
        }
        try {
            Logger.fine(CLASSNAME, "abrindo OutputStream");
            output = comm.openOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(output);
            Logger.fine(CLASSNAME, "enviando um caracter 'x'");
            writer.write('x');
            writer.flush();
            writer.close();
            writer = null;
            
            Logger.fine(CLASSNAME, "fechando OutputStream");
            Logger.fine(CLASSNAME, "abrindo InputStream");
            
            input = comm.openInputStream();
            
        } catch (Exception ex) {
            try {
                if (input != null) {
                    input.close();
                }
                if (comm != null) {
                    comm.close();
                }
                if (output != null) {
                    output.close();
                }
            } catch (IOException ex2) {
                Logger.error(CLASSNAME, "erro de I/O: " + ex2.getMessage());
            } finally {
                input = null;
                comm = null;
                output = null;
                Logger.error(CLASSNAME, "erro na serial " + ex.getMessage());
                return;
            }
        }
        
        int ch = -1;
        boolean nova_linha = false;
        try {
            StringBuffer sb = new StringBuffer();
            // Loop infinito. Em situacao normal nao deve nunca chegar ao final
            // da stream
            while ( (ch = input.read()) != -1) {
                if (ch == '/') {
                    break;
                } else if (ch == '\r') {
                    
                    // Se o StringBuffer tiver comprimento 0 significa uma linha em branco
                    try {
                        if (sb.toString().trim().length() == 0) {
                            obs.addReading(null);
                        } else {
                            obs.addReading(sb.toString().trim());
                            sb = new StringBuffer();
                        }
                    } catch (Exception e) {
                        Logger.error(CLASSNAME, "erro ao adiconar leitura: "+e.getMessage());
                        sb = new StringBuffer();
                    }
                }
                // Caracter de dado. Concatena no StringBuffer corrente
                else if (ch != '\n') {
                    sb.append((char) ch);
                } else if (ch == '\n') {
                }
            }
        } catch (Exception ex) {
            Logger.error(CLASSNAME, "erro: " + ex.getMessage());
        } finally {
            //MyWatchdog.serialState = false;
            try {
                System.out.println("Saiu leitor serial.");
                if (input != null) {
                    input.close();
                }
                if (comm != null) {
                    comm.close();
                }
                if (output != null) {
                    output.close();
                }
                
                input = null;
                comm = null;
                output = null;
            } catch (IOException ex) {
                Logger.error(CLASSNAME, "erro de I/O: " + ex.getMessage());
            }
        }
    }
    
}
