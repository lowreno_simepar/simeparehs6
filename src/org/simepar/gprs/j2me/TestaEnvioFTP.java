/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.gprs.j2me;

import java.util.TimerTask;

/**
 *
 * @author Daniel
 */
class TestaEnvioFTP extends TimerTask {

    String CLASSNAME;
    Manager tc65;
    Util util;

    public TestaEnvioFTP(Manager tc65, Util util) {
        this.CLASSNAME = this.getClass().getName();
        this.tc65 = tc65;
        this.util = util;
    }

    public void run() {
        MyWatchdog.ftpState = false;
        String log = "Envio atraso no ftp sender";
        String msg = "Telem - WARN " +
                        util.readProp(Util.FS_CONF, Util.CODESTACAO, false) + " - "+ log;
        Logger.warning(CLASSNAME, log);
        tc65.sendSms(util.readProp(Util.FS_CONF, Util.NUMSMS, false), msg);
        try {
            Thread.sleep(30000l);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
